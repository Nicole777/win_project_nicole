
// inject DLL to a running process

/*
in my code, I added some links that i used for learning some of the functions,
so it could serve me in the future, for example - when looking back at this code, and subject.
*/

#include "pch.h"
#include <iostream>
#include <string>
#include "windows.h"

#define EXIT 0

// print defined values
#define ERROR_PRINT_MSG "Error!!! "

// notepad's defined values
#define NOTEPAD_PATH "./Notepad.lnk"
#define PID 2868

// file's defined values
#define DLL_PATH "./mydll"

using std::cerr;
using std::endl;
using std::string;

int main()
{
	// Get full path of DLL to inject
	DWORD pathLen = GetFullPathNameA(NOTEPAD_PATH);

	DWORD processID = PID; //notepad.exe process ID - found using task maneger

	// Get LoadLibrary function address �
	// the address doesn't change at remote process
	// used doc. https://learn.microsoft.com/en-us/windows/win32/api/libloaderapi/nf-libloaderapi-getprocaddress
	PVOID addrLoadLibrary = (PVOID)GetProcAddress(GetModuleHandle(TEXT("kernel32.dll")),
		"LoadLibraryA");

	// Open remote process
	// used doc. https://learn.microsoft.com/en-us/windows/win32/api/processthreadsapi/nf-processthreadsapi-openprocess
	HANDLE proc = OpenProcess(
		PROCESS_ALL_ACCESS,
		false,
		processID); //modified according to a tip found on stackoverflow

	 
	// Get a pointer to memory location in remote process,
	// big enough to store DLL path
    // used doc. https://learn.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-virtualallocex
	PVOID memAddr = (PVOID)VirtualAllocEx(
		proc,
		0,
		strlen(string(DLL_PATH)),
		MEM_RESERVE | MEM_COMMIT,
		PAGE_READWRITE);

	if (NULL == memAddr) 
	{
		err = GetLastError();
		cerr << ERROR_PRINT_MSG << err << endl;
		return 0;
	}


	// Write DLL name to remote process memory
	// used doc. https://learn.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-writeprocessmemory
	BOOL check = WriteProcessMemory(
		proc,
		memAddr,
		DLL_PATH, 
		strlen(string(DLL_PATH)),
		NULL);


	if (0 == check) 
	{
		err = GetLastError();
		cerr << ERROR_PRINT_MSG << err << endl;
		return 0;
	}


	// Open remote thread, while executing LoadLibrary
	// with parameter DLL name, will trigger DLLMain
	// used doc. https://learn.microsoft.com/en-us/windows/win32/api/processthreadsapi/nf-processthreadsapi-createremotethread
	HANDLE hRemote = CreateRemoteThread(
		proc,
		0,
		0,
		(LPTHREAD_START_ROUTINE)addrLoadLibrary,
		memAddr,
		0,
		0);


	if (NULL == hRemote) 
	{
		err = GetLastError();
		cerr << ERROR_PRINT_MSG << err << endl;
		return 0;
	}


	WaitForSingleObject(hRemote, INFINITE);
	check = CloseHandle(hRemote);
	return EXIT;
}

